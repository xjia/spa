package spa

import scala.tools.nsc
import nsc.Global
import nsc.Phase
import nsc.plugins.Plugin
import nsc.plugins.PluginComponent

import scala.collection.mutable.LinkedList
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet

object Util {
  def log(colorIndex: Int, message: String) {
    println("\033[38;5;" + colorIndex + "m" + message);
  }

  def log(message: String) {
    log(8, message)
  }
}

trait Bijection[SourceT, DestT] {
  val forward = new HashMap[SourceT, DestT]();
  val backward = new HashMap[DestT, SourceT]();

  def genId: DestT

  def apply(x: SourceT): DestT =
    if (forward contains x)
      forward(x)
    else {
      val gid: DestT = genId
      forward.put(x, gid)
      backward.put(gid, x)
      Util.log("map " + x + " => " + gid)
      gid
    }

  def source(y: DestT): SourceT = backward(y)
}

object CallGraphGen {
  type ClassId = Int
  type MethodNameId = Int
  type MethodId = Int

  case class Subtype(subtype: ClassId, supertype: ClassId) {
    override def toString = subtype + " " + supertype
  }

  case class Method(method: MethodId, returnType: ClassId) {
    override def toString = method + " " + returnType
  }

  case class Param(method: MethodId, paramType: ClassId) {
    override def toString = method + " " + paramType
  }

  case class CallSite(method: MethodId, e: ClassId, m: MethodNameId) {
    override def toString = method + " " + e + " " + m
  }

  case class NewSite(method: MethodId, c: ClassId) {
    override def toString = method + " " + c
  }

  var subtypes = List[Subtype]()
  var methods = List[Method]()
  var params = List[Param]()
  var callSites = List[CallSite]()
  var newSites = List[NewSite]()

  object classIdOf extends Bijection[String, ClassId] {
    def genId = 1 + forward.size
  }

  object methodNameIdOf extends Bijection[String, MethodNameId] {
    def genId = 1 + forward.size
  }

  object methodIdOf extends Bijection[(ClassId, MethodNameId), MethodId] {
    def genId = 1 + forward.size
    def apply(className: String, methodName: String) =
      super.apply((classIdOf(className), methodNameIdOf(methodName)))
  }

  def emitSubtype(subtype: String, supertype: String) {
    Util.log(2, "emitSubtype( " + subtype + " <: " + supertype + " )")
    subtypes = Subtype(classIdOf(subtype), classIdOf(supertype)) :: subtypes
  }

  def emitMethod(className: String, methodName: String, returnType: String) {
    Util.log(2, "emitMethod( " + className + "." + methodName + ": return " + returnType + " )")
    methods = Method(methodIdOf(className, methodName), classIdOf(returnType)) :: methods
  }

  def emitMethodParam(className: String, methodName: String, paramType: String) {
    Util.log(2, "emitMethodParam( " + className + "." + methodName + ", paramType: " + paramType + " )")
    params = Param(methodIdOf(className, methodName), classIdOf(paramType)) :: params
  }

  def emitCallSite(className: String, methodName: String, e: String, m: String) {
    Util.log(2, "emitCallSite( " + className + "." + methodName + ": call " + e + "." + m + " )")
    callSites = CallSite(methodIdOf(className, methodName), classIdOf(e), methodNameIdOf(m)) :: callSites
  }

  def emitNewSite(className: String, methodName: String, c: String) {
    Util.log(2, "emitNewSite( " + className + "." + methodName + ": new " + c + " )")
    newSites = NewSite(methodIdOf(className, methodName), classIdOf(c)) :: newSites
  }

  object CallSites extends HashMap[MethodId, List[CallSite]] {
    def prepare() {
      for (callSite <- callSites) {
        if (contains(callSite.method)) {
          put(callSite.method, callSite :: this(callSite.method))
        } else {
          put(callSite.method, List(callSite))
        }
      }
    }

    override def apply(x: MethodId) = get(x) match {
      case Some(l) => l
      case None => List()
    }
  }

  object ClassMethods extends HashMap[ClassId, HashMap[MethodNameId, MethodId]] {
    def prepare() {
      for (method <- methods) {
        val (c, m) = methodIdOf.source(method.method)
        if (!contains(c)) {
          put(c, HashMap())
        }
        this(c).put(m, method.method)
      }
    }

    override def apply(x: ClassId) = get(x) match {
      case Some(m) => m
      case None => HashMap()
    }
  }

  object MethodName extends HashMap[MethodId, String] {
    def prepare() {
      for (method <- methods) {
        val (c, m) = methodIdOf.source(method.method)
        put(method.method, classIdOf.source(c) + "." + methodNameIdOf.source(m))
      }
    }

    override def apply(x: MethodId) = get(x) match {
      case Some(s) => s
      case None => "" + x
    }
  }

  object SuperTypes extends HashMap[ClassId, HashSet[ClassId]] {
    def prepare() {
      for (subtype <- subtypes) {
        if (!contains(subtype.subtype)) {
          put(subtype.subtype, HashSet())
        }
        this(subtype.subtype).add(subtype.supertype)
      }
    }

    override def apply(x: ClassId) = get(x) match {
      case Some(s) => s
      case None => HashSet()
    }
  }

  object SubTypes extends HashMap[ClassId, HashSet[ClassId]] {
    def prepare() {
      for (subtype <- subtypes) {
        if (!contains(subtype.supertype)) {
          put(subtype.supertype, HashSet())
        }
        this(subtype.supertype).add(subtype.subtype)
      }
    }

    override def apply(x: ClassId) = get(x) match {
      case Some(s) => s
      case None => HashSet()
    }
  }

  object StaticLookup extends HashMap[(ClassId, MethodNameId), Option[MethodId]] {
    def apply(C: ClassId, m: MethodNameId) = lookup(C, m)

    def lookup(C: ClassId, m: MethodNameId): Option[MethodId] = {
      val key = (C, m)
      if (!contains(key)) {
        put(key, realLookup(C, m))
      }
      return super.apply(key)
    }

    def realLookup(C: ClassId, m: MethodNameId): Option[MethodId] = {
      val cm = ClassMethods(C)
      if (cm contains m) {
        return Some(cm(m))
      }
      for (supertype <- SuperTypes(C)) {
        if (supertype != C) {
          lookup(supertype, m) match {
            case Some(method) => return Some(method)
            case None => ()
          }
        }
      }
      return None
    }
  }

  object InstantiatedClasses extends HashMap[MethodId, HashSet[ClassId]] {
    def prepare() {
      for (newSite <- newSites) {
        if (!contains(newSite.method)) {
          put(newSite.method, HashSet())
        }
        this(newSite.method).add(newSite.c)
      }
    }

    override def apply(x: MethodId) = get(x) match {
      case Some(s) => s
      case None => HashSet()
    }
  }

  /**
   * We can extend the constraint system for the reachability analysis (RA) to
   * also take class hierarchy information into account.  The result is known as
   * class hierarchy analysis (CHA).  We will use the notation StaticType(e) to
   * denote the static type of the expression e, SubTypes(t) to denote the set
   * of declared subtypes of type t, and the notation StaticLookup(C,m) to
   * denote the definition (if any) of a method with name m that one finds when
   * starting a static method lookup in the class C.  Like RA, CHA uses just one
   * set variable R ranging over sets of methods.  The constraints:
   *
   * 1. main \in R  (main denotes the main method in the program)
   *
   * 2. For each method M, each virtual call site e.m(...) occurring in M, and
   *    each class C \in SubTypes(StaticType(e)) where StaticLookup(C,m) = M':
   *    (M \in R) => (M' \in R).
   *
   * Intuitively, the second constraint reads: "if a method is reachable, and a
   * virtual method call e.m(...) occurs in the body of that method, then every
   * method with name m that is inherited by a subtype of the static type of e
   * is also reachable."
   */
  object ClassHierarchyAnalysis {
    def calculateClosure() {
      var set = new HashSet[Subtype]()
      for (subtype <- subtypes) {
        set.add(subtype)
      }
      var stop = false
      while (!stop) {
        var nextset = new HashSet[Subtype]()
        for (relation <- set) {
          nextset.add(relation)
        }
        for (r1 <- set) {
          for (r2 <- set) {
            if (r1 != r2 && r1.subtype == r2.supertype) {
              // Util.log("nextset.add: " + r2.subtype + " <: " + r1.supertype)
              nextset.add(Subtype(r2.subtype, r1.supertype))
            }
          }
        }
        if (nextset.equals(set)) {
          stop = true
        } else {
          set = nextset
        }
      }
      // Util.log(1, "before: " + subtypes)
      subtypes = List[Subtype]()
      for (subtype <- set) {
        subtypes = subtype :: subtypes
      }
      // Util.log(1, "after: " + subtypes)
    }
    
    def apply(main: MethodId) {
      // FIXME turn this into preparation dependency
      CallSites.prepare()
      ClassMethods.prepare()
      MethodName.prepare()
      calculateClosure();
      SuperTypes.prepare()
      SubTypes.prepare()

      val R = HashSet[MethodId]()
      R.add(main)

      for (method <- iterate(R)) {
        Util.log(255, MethodName(method))
      }
    }

    def iterate(R: HashSet[MethodId]): HashSet[MethodId] = {
      val R1 = R.clone
      for (M <- R) {
        for (CallSite(_, e, m) <- CallSites(M)) {
          for (C <- SubTypes(e)) {
            StaticLookup(C, m) match {
              case Some(method) => R1.add(method)
              case None => ()
            }
          }
        }
      }
      if (R1 equals R)
        R
      else
        iterate(R1)
    }
  }

  /**
   * We can further extend CHA to take class-instantiation information into
   * account.  The result is known as rapid type analysis (RTA).  RTA uses both
   * a set variable R ranging over sets of methods, and a set variable S which
   * ranges over sets of class names.  The variable S approximates the set of
   * classes for which objects are created during a run of the problem.  The
   * constraints:
   *
   * 1. main \in R  (main denotes the main method in the program)
   *
   * 2. For each method M, each virtual call site e.m(...) occurring in M, and
   *    each class C \in SubTypes(StaticType(e)) where StaticLookup(C,m) = M':
   *    (M \in R) and (C \in S) => (M' \in R).
   *
   * 3. For each method M, and for each "new C()" occurring in M:
   *    (M \in R) => (C \in S).
   *
   * Intuitively, the second constraint refines the corresponding constraint of
   * CHA by insisting that C \in S, and the third constraint reads: "S contains
   * the classes that are instantiated in a reachable method."
   */
  object RapidTypeAnalysis {
    def apply(main: MethodId) {
      // FIXME turn this into preparation dependency
      CallSites.prepare()
      ClassMethods.prepare()
      MethodName.prepare()
      SuperTypes.prepare()
      SubTypes.prepare()
      InstantiatedClasses.prepare()

      val r = HashSet[MethodId]()
      val s = HashSet[ClassId]()
      r.add(main)
      val (r1, _) = iterate(r, s)
      for (method <- r1) {
        Util.log(MethodName(method))
      }
    }

    def iterate(r: HashSet[MethodId], s: HashSet[ClassId]): (HashSet[MethodId], HashSet[ClassId]) = {
      Util.log("R=" + r)
      Util.log("S=" + s)
      val r1 = r.clone
      val s1 = s.clone
      for (M <- r) {
        for (c <- InstantiatedClasses(M)) {
          s1.add(c)
        }
        for (CallSite(_, e, m) <- CallSites(M)) {
          Util.log("e=" + e + " m=" + m)
          for (c <- s1 & SubTypes(e)) {
            StaticLookup(c, m) match {
              case None => ()
              case Some(m1) => {
                r1.add(m1)
                for (c <- InstantiatedClasses(m1)) {
                  s1.add(c)
                }
              }
            }
          }
        }
      }
      if ((r1 equals r) && (s1 equals s))
        (r, s)
      else
        iterate(r1, s1)
    }
  }

  def apply(entry: String, algorithm: String) {
    Util.log(33, "entry: " + entry + "\talgorithm: " + algorithm)
    Util.log(33, "subtypes: " + subtypes)
    Util.log(33, "methods: " + methods)
    Util.log(33, "params: " + params)
    Util.log(33, "callSites: " + callSites)
    Util.log(33, "newSites: " + newSites)

    val main = methodIdOf(entry, "main")
    Util.log(33, "main: " + main)

    if (algorithm equals "CHA") {
      ClassHierarchyAnalysis(main)
    } else if (algorithm equals "RTA") {
      RapidTypeAnalysis(main)
    } else if (algorithm equals "XTA") {
      ???
    }
  }
}

class CallGraphPlugin(val global: Global) extends Plugin {
  import global._

  val name = "callgraph"
  val description = "call graph generation"
  val components = List[PluginComponent](Component)

  var entry = ""
  var algorithm = ""

  override def processOptions(options: List[String], error: String => Unit) {
    for (option <- options) {
      if (option startsWith "entry:") {
        entry = option.substring("entry:".length)
      } else if (option startsWith "algorithm:") {
        algorithm = option.substring("algorithm:".length)
      } else {
        error("Option not understood: " + option)
      }
    }
  }

  private object Component extends PluginComponent {
    val global: CallGraphPlugin.this.global.type = CallGraphPlugin.this.global
    val runsAfter = List[String]("refchecks")
    val phaseName = CallGraphPlugin.this.name
    def newPhase(prev: Phase) = new CallGraphPluginPhase(prev)

    class CallGraphPluginPhase(prev: Phase) extends StdPhase(prev) {
      override def name = CallGraphPlugin.this.name

      def apply(unit: CompilationUnit) {
        if (entry equals "") {
          error("no entry specified")
          return
        }

        if (algorithm equals "") {
          error("no algorithm specified")
          return
        } else if (algorithm equals "CHA") {
          Util.log(33, "using class hierarchy analysis (-P:callgraph:algorithm:CHA)")
        } else if (algorithm equals "RTA") {
          Util.log(33, "using rapid type analysis (-P:callgraph:algorithm:RTA)")
        } else if (algorithm equals "XTA") {
          error("XTA is not yet implemented (-P:callgraph:algorithm:XTA)")
          return
        } else {
          error("unknown algorithm: " + algorithm)
          return
        }

        for (cd @ ClassDef(_, _, _, Template(cParents, _, cBody)) <- unit.body) {
          val className = cd.symbol.fullName
          CallGraphGen.emitSubtype(className, className)
          for (parent <- cParents) {
            CallGraphGen.emitSubtype(className, parent.symbol.fullName)
          }
          def handle2(parentMethodName: String, t: Tree): Unit = t match {
            case DefDef(_, mName, _, mParamss, mReturn, mBody) => {
              val methodName = parentMethodName + "~" + mName
              CallGraphGen.emitMethod(className, methodName, mReturn.tpe.typeSymbol.fullName)
              for (mParams <- mParamss; ValDef(_, _, tpt, _) <- mParams) {
                CallGraphGen.emitMethodParam(className, methodName, tpt.tpe.typeSymbol.fullName)
              }
              mBody.children.foreach(child => handle2(methodName, child))
            }
            case Apply(Ident(m), rest) => {
              var owner = t.symbol.owner
              var prefix = ""
              while (owner.isMethod) {
                prefix = owner.name + "~" + prefix
                owner = owner.owner;
              }
              CallGraphGen.emitCallSite(className, parentMethodName, owner.fullName, prefix + m)
              rest.foreach(child => handle2(parentMethodName, child))
            }
            case Apply(Select(e, m), rest) => {
              CallGraphGen.emitCallSite(className, parentMethodName, e.tpe.typeSymbol.fullName, m.toString)
              handle2(parentMethodName, e)
              rest.foreach(child => handle2(parentMethodName, child))
            }
            case New(newC) => {
              CallGraphGen.emitNewSite(className, parentMethodName, newC.tpe.typeSymbol.fullName)
            }
            case Ident(m) => {
              if (t.symbol.isMethod) {
                var owner = t.symbol.owner
                var prefix = ""
                while (owner.isMethod) {
                  prefix = owner.name + "~" + prefix
                  owner = owner.owner;
                }
                CallGraphGen.emitCallSite(className, parentMethodName, owner.fullName, prefix + m)
              }
            }
            case _ => {
              t.children.foreach(child => handle2(parentMethodName, child))
            }
          }
          def handle1(t: Tree): Unit = t match {
            case DefDef(_, mName, _, mParamss, mReturn, mBody) => {
              val methodName = mName.toString
              CallGraphGen.emitMethod(className, methodName, mReturn.tpe.typeSymbol.fullName)
              for (mParams <- mParamss; ValDef(_, _, tpt, _) <- mParams) {
                CallGraphGen.emitMethodParam(className, methodName, tpt.tpe.typeSymbol.fullName)
              }
              mBody.children.foreach(child => handle2(methodName, child))
            }
            case _ => {
              t.children.foreach(child => handle1(child))
            }
          }
          cBody.foreach(child => handle1(child))
        }

        CallGraphGen(entry, algorithm)
      }
    }
  }
}

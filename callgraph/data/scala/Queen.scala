 object Queen {//extends Application{  
     def main() {
    return solve()      
}
 
    def solve(){  
        var count =0  

        def solve(ls:List[(Int,Int)]):Unit = {  
            if( ls.size == 8 ) printAnswer(ls)  
            else{  
                var s =ls.size  
                for( t <- 0 until 8 )  
                    if(ls.forall(i => {  
                        var (x,y) =i  
                        !(x==s||y==t||x+y==t+s||s-x==t-y)  
                       }))  solve((s,t)::ls)  
            }  
        }  

        def printAnswer(an:List[(Int,Int)]){  
            count += 1
            println("\n#Answer "+count)
            for(r <- 0 until 8 ){  
                for(c <- 0 until 8 ) print(if(an(7-r)._2==c) 'o' else 'x')
                println()  
            }  
        }  
        solve(Nil)  
    }  
}  

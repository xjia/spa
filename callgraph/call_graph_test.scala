// Results of different algorithms:
// - RA knows *.notCalled is not used
// - CHA knows NotChild.sayHello is not used
// - RTA knows Parent.sayHello and Child3.sayHello are not used
// - XTA knows ???
// - ??? knows Child1.sayHello is not used

class Parent {
  def notCalled() = println("Parent.notCalled")
  def sayHello() = println("Hello from Parent")
}

class Child1 extends Parent {
  override def notCalled() = println("Child1.notCalled")
  override def sayHello() = println("Hello from Child1")
}

class Child2 extends Parent {
  override def notCalled() = println("Child2.notCalled")
  override def sayHello() = println("Hello from Child2")
}

class Child3 extends Parent {
  override def notCalled() = println("Child3.notCalled")
  override def sayHello() = println("Hello from Child3")
}

class NotChild {
  def notCalled() = println("NotChild.notCalled")
  def sayHello() = println("Hello from NotChild")
}

object CallGraphTest {
  def createObject1(): Parent = new Child1()
  def createObject2(): Parent = new Child2()

  def main(args: Array[String]) {
    createObject1()
    createObject2().sayHello()
  }
}

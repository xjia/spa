Scala Program Analyzer
======================

Information Needed
------------------

- Entry point: the `main` of *which class*
- All inheritances, i.e. (class, sub-class) relation
- All methods, i.e. (class, method *the* return type) relation
- All method parameter types, i.e. (class, method, a param type) relation
- All virtual call sites, i.e. (class, method, static type of e, name of m) relation
- All "new C()" sites, i.e. (class, method, C) relation

Notes:

- The entry point should be specified by the user.
- Since it is impossible for a static analysis to determine which classes may be accessed using mechanisms such as reflection and dynamic loading, we have to manually supply the analysis with information about what classes are used by these mechanisms.
- In CHA, StaticLookup(C,m) denotes the definition of a method with name m that one finds when starting a static method lookup in the class C.  C itself does not necessarily define m, since C's ancestors may define m.
- XTA needs information about fields, so we should add more information needed, and extend the `call_graph_test.scala` program.

Use following commands to investigate the annotated AST:

    scalac call_graph_test.scala -Ybrowse:refchecks
    scalac call_graph_test.scala -Ybrowse:parser   
    scalac call_graph_test.scala -Ybrowse:typer 

Scala Compiler Phases
---------------------

        phase name  id  description
        ----------  --  -----------
            parser  1   parse source into ASTs, perform simple desugaring
             namer  2   resolve names, attach symbols to named trees
    packageobjects  3   load package objects
             typer  4   the meat and potatoes: type the trees
    superaccessors  5   add super accessors in traits and nested classes
           pickler  6   serialize symbol tables
         refchecks  7   reference/override checking, translate nested objects
          liftcode  8   reify trees
           uncurry  9   uncurry, translate function values to anonymous classes
         tailcalls  10  replace tail calls by jumps
        specialize  11  @specialized-driven class and method specialization
     explicitouter  12  this refs to outer pointers, translate patterns
           erasure  13  erase types, add interfaces for traits
          lazyvals  14  allocate bitmaps, translate lazy vals into lazified defs
        lambdalift  15  move nested functions to top level
      constructors  16  move field definitions into constructors
           flatten  17  eliminate inner classes
             mixin  18  mixin composition
           cleanup  19  platform-specific cleanups, generate reflective calls
             icode  20  generate portable intermediate code
           inliner  21  optimization: do inlining
          closelim  22  optimization: eliminate uncalled closures
               dce  23  optimization: eliminate dead code
               jvm  24  generate JVM bytecode
          terminal  25  The last phase in the compiler chain

References
----------

- http://www.scala-lang.org/files/archive/api/2.10.3/#scala.reflect.api.Symbols$ClassSymbol
- http://www.scala-lang.org/files/archive/api/2.10.3/index.html#scala.reflect.api.Types$Type
- [Writing Scala Compiler Plugins](http://www.scala-lang.org/old/node/140)
  - Get AST information
- [scala.tools.nsc.ast.NodePrinters](https://github.com/scala/scala/blob/master/src/compiler/scala/tools/nsc/ast/NodePrinters.scala)
  - Pattern match AST nodes
- [scala.reflect.internal.Trees](https://github.com/scala/scala/blob/master/src/reflect/scala/reflect/internal/Trees.scala)
  - AST internals
- [The Scala Compiler Corner](http://lampwww.epfl.ch/~magarcia/ScalaCompilerCornerReloaded/)
  - Scala compiler phases
- Frank Tip, Jens Palsberg. *Scalable Propagation-Based Call Graph Construction Algorithms.* OOPSLA'00.
  - CHA, RTA, XTA, 0-CFA
- David F. Bacon, Peter F. Sweeney. *Fast static analysis of C++ virtual function calls.* OOPSLA'96.
  - RTA
